---
features:
  primary:
  - name: "OpenID Connect support for GitLab CI/CD"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/ci/cloud_services'
    reporter: nagyv-gitlab
    stage: configure
    categories:
    - Secrets Management
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/29047'
    image_url: /images/unreleased/oidc.drawio.png
    description: |
      Connecting GitLab CI/CD to cloud providers using environment variables works fine for many use cases but does not scale well if you need advanced permissions management or would prefer a signed, short-lived, contextualized connection to your cloud provider. GitLab 12.10 shipped initial support for JWT token-based connection (`CI_JOB_JWT`) to enable HashiCorp Vault users to safely retrieve secrets. That implementation was restricted to Vault, while the logic we built JWT upon opened up the possibility to connect to other providers as well. In GitLab 14.7, we are introducing a [`CI_JOB_JWT_V2`](https://docs.gitlab.com/ee/ci/cloud_services/) environment variable that can be used to connect to AWS, GCP, Vault, and likely many other cloud services.

      With the new `CI_JOB_JWT_V2` variable you can connect to AWS to retrieve secrets or to deploy within your account. You can manage access rights to your cluster using AWS IAM roles. We have documentation on setting up OIDC connection with [AWS](https://docs.gitlab.com/ee/ci/cloud_services/aws/), GCP, and Azure.

      The new variable is automatically injected into your pipeline but is not backward compatible with the current `CI_JOB_JWT`. Until GitLab 15.0, the `CI_JOB_JWT` will continue to work normally but this will change in a [future release](https://gitlab.com/gitlab-org/gitlab/-/issues/349110). We'll keep you posted. The `secrets` stanza today uses the `CI_JOB_JWT_V1` variable. If you use the `secrets` stanza, you don't have to make any changes yet. We [plan to change this behaviour in a future release](https://gitlab.com/gitlab-org/gitlab/-/issues/349110), and we will notify you about the change in time.
