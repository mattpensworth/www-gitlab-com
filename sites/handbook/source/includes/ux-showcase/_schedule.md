[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date ('22) | Host                  |
| ---------- | --------------------- |
| 2022-01-19 | Justin                |
| 2022-02-02 | Taurie                |
| 2022-02-16 | APAC/Europe  (Marcel) |
| 2022-03-02 | Marcel                |
| 2022-03-16 | Rayana                |
| 2022-03-30 | Blair                 |
| 2022-04-13 | Jacki                 |
| 2022-04-27 | Justin                |
| 2022-05-11 | Taurie                |
| 2022-05-25 | APAC/Europe  (Rayana) |
| 2022-06-08 | Marcel                |
| 2022-06-22 | Rayana                |
| 2022-07-06 | Blair                 |
| 2022-07-20 | Jacki                 |
| 2022-08-03 | Justin                |
| 2022-08-17 | Taurie                |
| 2022-08-31 | APAC/Europe  (Marcel) |
| 2022-09-14 | Marcel                |
| 2022-09-28 | Rayana                |
| 2022-10-12 | Blair                 |
| 2022-10-26 | Jacki                 |
| 2022-11-09 | Justin                |
| 2022-11-23 | Taurie                |
| 2022-12-07 | APAC/Europe  (Rayana) |
| 2022-12-21 | Marcel                |
